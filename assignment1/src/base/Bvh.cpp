#include "Bvh.hpp"
#include "filesaves.hpp"
#include <numeric>
#include <algorithm>
#include <wincrypt.h>


namespace FW
{
    Bvh::Bvh()
    {
    }

    // reconstruct from a file
    Bvh::Bvh(std::istream& is)
    {
        // Load file header.
        fileload(is, mode_);
        Statusbar nodeInfo("Loading nodes", 0);
        Loader loader(is, nodeInfo);

        // Load elements.
        {
            size_t size;
            fileload(is, size);

            for (size_t i = 0; i < size; ++i)
            {
                uint32_t idx;
                loader(idx);
                indices_.push_back(idx);
            }
        }

        // Load the rest.
        rootNode_.reset(new BvhNode(loader));
    }

    void Bvh::save(std::ostream& os)
    {
        // Save file header.
        filesave(os, mode_);
        Statusbar nodeInfo("Saving nodes", 0);
        Saver saver(os, nodeInfo);

        // Save elements.
        {
            filesave(os, (size_t)indices_.size());

            for (auto& i : indices_)
            {
                saver(i);
            }
        }

        // Save the rest.
        rootNode_->save(saver);
    }

    /*
    *
    *   MY CODE HERE
    *
    */

    void Bvh::init(const std::vector<RTTriangle>& triangles, std::unique_ptr<BvhNode> root)
    {
        indices_.resize(triangles.size());
        std::iota(indices_.begin(), indices_.end(), 0);
        triangles_ = triangles;
        for (int i = 0; i < triangles_.size(); i++)
        {
            AABB box = computeTriBB(i);
            BBCentroids.push_back(BBCentroid(box));
        }
        rootNode_ = std::move(root);
    }

    void Bvh::partitionPrimitives(const BvhNode& node, int& rightStart, const SplitMode& mode)
    {
        std::vector<uint32_t>::iterator rightStartIt;
        switch (mode)
        {
        case FW::SplitMode_ObjectMedian:
            if (abs(node.bb.max.x - node.bb.min.x) > abs(node.bb.max.y - node.bb.min.y) &&
                abs(node.bb.max.x - node.bb.min.x) > abs(node.bb.max.z - node.bb.min.z))
            {
                std::sort(indices_.begin() + node.startPrim, indices_.begin() + node.endPrim,
                          [this](int a, int b)
                          {
                              return BBCentroids[a].x < BBCentroids[b].x;
                          });
            }
            else if (abs(node.bb.max.y - node.bb.min.y) > abs(node.bb.max.x - node.bb.min.x) &&
                abs(node.bb.max.y - node.bb.min.y) > abs(node.bb.max.z - node.bb.min.z))
            {
                std::sort(indices_.begin() + node.startPrim, indices_.begin() + node.endPrim,
                          [this](int a, int b)
                          {
                              return BBCentroids[a].y < BBCentroids[b].y;
                          });
            }
            else
            {
                std::sort(indices_.begin() + node.startPrim, indices_.begin() + node.endPrim,
                          [this](int a, int b)
                          {
                              return BBCentroids[a].z < BBCentroids[b].z;
                          });
            }
            rightStart = node.startPrim + (node.endPrim - node.startPrim) / 2;
            break;
        default:
            break;
        }
    }

    AABB Bvh::computeTriBB(int index) const
    {
        auto boxMin = Vec3f(FLT_MAX);
        auto boxMax = Vec3f(-FLT_MAX);
        auto tri = triangles_[getIndex(index)];
        for (auto v : tri.m_vertices)
        {
            if (v.p.x < boxMin.x)
                boxMin.x = v.p.x;
            if (v.p.y < boxMin.y)
                boxMin.y = v.p.y;
            if (v.p.z < boxMin.z)
                boxMin.z = v.p.z;
            if (v.p.x > boxMax.x)
                boxMax.x = v.p.x;
            if (v.p.y > boxMax.y)
                boxMax.y = v.p.y;
            if (v.p.z > boxMax.z)
                boxMax.z = v.p.z;
        }
        return AABB(boxMin, boxMax);
    }

    Vec3f Bvh::BBCentroid(AABB& box)
    {
        auto a = box.min;
        auto b = box.max;
        auto c = Vec3f(box.min.x, box.min.y, box.max.z);
        auto d = Vec3f(box.min.x, box.max.y, box.min.z);
        auto e = Vec3f(box.min.x, box.max.y, box.max.z);
        auto f = Vec3f(box.max.x, box.min.y, box.max.z);
        auto g = Vec3f(box.max.x, box.max.y, box.min.z);
        auto h = Vec3f(box.max.x, box.min.y, box.min.z);

        return (a + b + c + d + e + f + g + h) * (1.0f / 8.0f);
    }

    AABB Bvh::computeBB(int start, int end) const
    {
        auto boxMin = Vec3f(FLT_MAX);
        auto boxMax = Vec3f(-FLT_MAX);
        for (auto i = start; i < end; i++)
        {
            const auto triBB = computeTriBB(i);
            if (triBB.min.x < boxMin.x)
                boxMin.x = triBB.min.x;
            if (triBB.min.y < boxMin.y)
                boxMin.y = triBB.min.y;
            if (triBB.min.z < boxMin.z)
                boxMin.z = triBB.min.z;

            if (triBB.max.x > boxMax.x)
                boxMax.x = triBB.max.x;
            if (triBB.max.y > boxMax.y)
                boxMax.y = triBB.max.y;
            if (triBB.max.z > boxMax.z)
                boxMax.z = triBB.max.z;
        }
        return AABB(boxMin, boxMax);
    }

    void Bvh::constructTree(BvhNode& node)
    {
        node.bb = computeBB(node.startPrim, node.endPrim);
        node.left = nullptr;
        node.right = nullptr;
        if (node.endPrim - node.startPrim > MAX_TRIS_PER_LEAF)
        {
            auto rightStart = 0;
            partitionPrimitives(node, rightStart, mode_);
            node.left.reset(new BvhNode(0, node.startPrim, rightStart));
            constructTree(*node.left);
            node.right.reset(new BvhNode(0, rightStart, node.endPrim));
            constructTree(*node.right);
        }
    }

    void Bvh::setMode(const SplitMode& mode)
    {
        mode_ = mode;
    }
}
