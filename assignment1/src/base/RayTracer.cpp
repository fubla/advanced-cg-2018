#define _CRT_SECURE_NO_WARNINGS

#include "base/Defs.hpp"
#include "base/Math.hpp"
#include "RayTracer.hpp"
#include <stdio.h>
#include "rtIntersect.inl"
#include <fstream>

#include "rtlib.hpp"


// Helper function for hashing scene data for caching BVHs
extern "C" void MD5Buffer(void* buffer, size_t bufLen, unsigned int* pDigest);


namespace FW
{
#define EPSILON 0.001f

    Vec2f getTexelCoords(Vec2f uv, const Vec2i size)
    {
        // YOUR CODE HERE (R3):
        // Get texel indices of texel nearest to the uv vector. Used in texturing.
        // UV coordinates range from negative to positive infinity. First map them
        // to a range between 0 and 1 in order to support tiling textures, then
        // scale the coordinates by image resolution and find the nearest pixel.
        auto u = uv.x - 1.0f * floor( uv.x );
        auto v = uv.y - 1.0f * floor( uv.y );
        return Vec2f(size)*Vec2f(u,v);
    }

    Mat3f formBasis(const Vec3f& n)
    {
        // YOUR CODE HERE (R4):
        Mat3f R;
        R.setCol(2, n);
        Vec3f Q = n;
        auto absX = fabs(Q.x);
        auto absY = fabs(Q.y);
        auto absZ = fabs(Q.z);

        if(absX < absY && absX < absZ)
            Q.x = 1.0f;
        else if (absY < absZ && absY < absZ)
            Q.y = 1.0f;
        else
            Q.z = 1.0f;
        auto T = cross(Q, n).normalized();
        auto B = cross(n, T);
        R.setCol(0, T);
        R.setCol(1, B);
        return R;
    }


    String RayTracer::computeMD5(const std::vector<Vec3f>& vertices)
    {
        unsigned char digest[16];
        MD5Buffer((void*)&vertices[0], sizeof(Vec3f) * vertices.size(), (unsigned int*)digest);

        // turn into string
        char ad[33];
        for (int i = 0; i < 16; ++i)
            ::sprintf(ad + i * 2, "%02x", digest[i]);
        ad[32] = 0;

        return FW::String(ad);
    }


    // --------------------------------------------------------------------------


    RayTracer::RayTracer()
    {
    }

    RayTracer::~RayTracer()
    {
    }


    void RayTracer::loadHierarchy(const char* filename, std::vector<RTTriangle>& triangles)
    {
        std::ifstream ifs(filename, std::ios::binary);
        bvh = std::move(Bvh(ifs));

        m_triangles = &triangles;
    }

    void RayTracer::saveHierarchy(const char* filename, const std::vector<RTTriangle>& triangles)
    {
        (void)triangles; // Not used.

        std::ofstream ofs(filename, std::ios::binary);
        bvh.save(ofs);
    }

    void RayTracer::constructHierarchy(std::vector<RTTriangle>& triangles, SplitMode splitMode)
    {
        // YOUR CODE HERE (R1):
        bvh.init(triangles, std::make_unique<BvhNode>(0, 0, triangles.size()));
        bvh.setMode(FW::SplitMode_ObjectMedian);
        bvh.constructTree(bvh.root());
        m_triangles = &triangles;
    }

    RaycastResult RayTracer::raycast(const Vec3f& orig, const Vec3f& dir) const
    {
        ++m_rayCount;

        // YOUR CODE HERE (R1):
        // This is where you hierarchically traverse the tree you built!
        // You can use the existing code for the leaf nodes.

        // precalculated ray direction inverses
        float rdx = 1 / dir.x;
        float rdy = 1 / dir.y;
        float rdz = 1 / dir.z;

        float tmin = NO_INTERSECT, umin = 0.0f, vmin = 0.0f;
        int imin = -1;

        const BvhNode& node = bvh.root();
        RaycastResult castresult;

        // intersect scene bounding box
        auto boxMin = intersectBox(node, orig, dir, rdx, rdy, rdz);
        if (abs(boxMin - NO_INTERSECT) < EPSILON)
        {
            return castresult;
        }
        tmin = traverseTree(node, orig, dir, rdx, rdy, rdz, tmin, umin, vmin, imin);

        if (imin != -1)
        {
            castresult = RaycastResult(&(*m_triangles)[bvh.getIndex(imin)], tmin, umin, vmin, orig + tmin * dir,
                                       orig,
                                       dir);
        }
        return castresult;
    }

    // returns intersection with bounding box
    float RayTracer::intersectBox(const BvhNode& node, const Vec3f& orig, const Vec3f& dir, float rdx, float rdy,
                                  float rdz) const
    {
        // missed bounding box
        if ((dir.x == 0 && (orig.x < node.bb.min.x || orig.x > node.bb.max.x)) ||
            (dir.y == 0 && (orig.y < node.bb.min.y || orig.z > node.bb.max.y)) ||
            (dir.z == 0 && (orig.z < node.bb.min.z || orig.z > node.bb.max.z))
        )
        {
            return NO_INTERSECT;
        }

        // for each dimension
        // first x
        float t1 = (node.bb.min.x - orig.x) * rdx;
        float t2 = (node.bb.max.x - orig.x) * rdx;

        if (t2 < t1)
        {
            float tmp = t2;
            t2 = t1;
            t1 = tmp;
        }

        float tStart = t1;
        float tEnd = t2;

        // then y
        t1 = (node.bb.min.y - orig.y) * rdy;
        t2 = (node.bb.max.y - orig.y) * rdy;

        if (t2 < t1)
        {
            float tmp = t2;
            t2 = t1;
            t1 = tmp;
        }

        if (t1 > tStart)
            tStart = t1;
        if (t2 < tEnd)
            tEnd = t2;

        // finally z
        t1 = (node.bb.min.z - orig.z) * rdz;
        t2 = (node.bb.max.z - orig.z) * rdz;

        if (t2 < t1)
        {
            float tmp = t2;
            t2 = t1;
            t1 = tmp;
        }

        if (t1 > tStart)
            tStart = t1;
        if (t2 < tEnd)
            tEnd = t2;

        // box is missed or behind
        if (tStart > tEnd || tEnd < T_MINIMUM || tStart > dir.length())
        {
            return NO_INTERSECT;
        }

        // closest intersection at tStart
        if (tStart > T_MINIMUM)
        {
            return tStart;
        }

        // eye inside box
        return T_MINIMUM;
    }

    float RayTracer::intersectPrimitives(const BvhNode& node, const Vec3f& orig, const Vec3f& dir,
                                         float tmin, float& umin, float& vmin, int& imin) const
    {
        for (size_t i = node.startPrim; i < node.endPrim; ++i)
        {
            float t, u, v;
            if ((*m_triangles)[bvh.getIndex(i)].intersect_woop(orig, dir, t, u, v))
            {
                if (t > 0.0f && t < tmin && t <= dir.length())
                {
                    imin = i;
                    umin = u;
                    vmin = v;
                    tmin = t;
                }
            }
        }
        return tmin;
    }

    float RayTracer::traverseTree(const BvhNode& node, const Vec3f& orig, const Vec3f& dir, float rdx, float rdy,
                                  float rdz, const float tmin, float& umin, float& vmin, int& imin) const
    {
        // if at leaf node, traverse primitives
        if (node.left == nullptr && node.right == nullptr)
        {
            return intersectPrimitives(node, orig, dir, tmin, umin, vmin, imin);
        }

        if (node.left == nullptr)
        {
            auto boxMin = intersectBox(*node.right, orig, dir, rdx, rdy, rdz);
            if (boxMin > tmin || abs(boxMin - NO_INTERSECT) < EPSILON)
            {
                return tmin;
            }
            return min(traverseTree(*node.right, orig, dir, rdx, rdy, rdz, tmin, umin, vmin, imin), tmin);
        }
        if (node.right == nullptr)
        {
            auto boxMin = intersectBox(*node.left, orig, dir, rdx, rdy, rdz);
            if (boxMin > tmin || abs(boxMin - NO_INTERSECT) < EPSILON)
            {
                return tmin;
            }
            return min(traverseTree(*node.left, orig, dir, rdx, rdy, rdz, tmin, umin, vmin, imin), tmin);
        }

        auto boxR = intersectBox(*node.right, orig, dir, rdx, rdy, rdz);
        if (boxR > tmin || abs(boxR - NO_INTERSECT) < EPSILON)
        {
            auto tLeft = traverseTree(*node.left, orig, dir, rdx, rdy, rdz, tmin, umin, vmin, imin);
            return min(tLeft, tmin);
        }
        auto boxL = intersectBox(*node.left, orig, dir, rdx, rdy, rdz);
        if (boxL > tmin || abs(boxL - NO_INTERSECT) < EPSILON)
        {
            auto tRight = traverseTree(*node.right, orig, dir, rdx, rdy, rdz, tmin, umin, vmin, imin);
            return min(tRight, tmin);
        }

        // check closer child
        if (boxR < boxL)
        {
            auto tRight = traverseTree(*node.right, orig, dir, rdx, rdy, rdz, tmin, umin, vmin, imin);
            // if closest intersection lies inside further child's bb, check that child
            if (boxL < tRight)
            {
                auto tLeft = traverseTree(*node.left, orig, dir, rdx, rdy, rdz, tRight, umin, vmin, imin);
                if (tLeft < tRight)
                    return min(tLeft, tmin);
            }
            return min(tRight, tmin);
        }
        else
        {
            auto tLeft = traverseTree(*node.left, orig, dir, rdx, rdy, rdz, tmin, umin, vmin, imin);
            // if closest intersection lies inside further child's bb, check that child
            if (boxR < tLeft)
            {
                auto tRight = traverseTree(*node.right, orig, dir, rdx, rdy, rdz, tLeft, umin, vmin, imin);
                if (tRight < tLeft)
                    return min(tRight, tmin);
            }
            return min(tLeft, tmin);
        }
    }
} // namespace FW
