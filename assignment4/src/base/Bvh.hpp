#pragma once


#include "BvhNode.hpp"


#include <vector>
#include <iostream>
#include <memory>


namespace FW {


class Bvh {
public:

    Bvh();
    Bvh(std::istream& is);

    // move assignment for performance
    Bvh& operator=(Bvh&& other) {
        mode_ = other.mode_;
        std::swap(rootNode_, other.rootNode_);
        std::swap(indices_, other.indices_);
        return *this;
    }

    BvhNode&			root() { return *rootNode_; }
    const BvhNode&		root() const { return *rootNode_; }

    void				save(std::ostream& os);

	uint32_t			getIndex(uint32_t index) const { return indices_[index]; }

    // contruction of BVH
    void constructTree(BvhNode& node);

    // set splitmode
    void setMode(const SplitMode& mode);

    // initialize indices list
    void init(const std::vector<RTTriangle>& triangles, std::unique_ptr<BvhNode> root);

private:

    std::vector<RTTriangle> triangles_;
    SplitMode						mode_;
    std::unique_ptr<BvhNode>		rootNode_;
    
	std::vector<uint32_t>			indices_; // triangle index list that will be sorted during BVH construction

    const int MAX_TRIS_PER_LEAF = 4;
    
    void partitionPrimitives(const BvhNode& node, int& rightStart, const SplitMode& mode);
    
    AABB computeTriBB(int index) const;
    AABB computeBB(int start, int end) const;

    static Vec3f BBCentroid(AABB& box);
    std::vector<Vec3f> BBCentroids;
};


}