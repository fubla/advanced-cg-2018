#include "PathTraceRenderer.hpp"
#include "RayTracer.hpp"
#include "AreaLight.hpp"

#include <atomic>
#include <chrono>
#include <string>
#include <stack>


namespace FW
{
	bool PathTraceRenderer::m_normalMapped = false;
	bool PathTraceRenderer::debugVis = false;

	void PathTraceRenderer::getTextureParameters(const RaycastResult& hit, Vec3f& diffuse, Vec3f& n, Vec3f& specular)
	{
		const float gamma = 2.2;
		MeshBase::Material* mat = hit.tri->m_material;
		// YOUR CODE HERE (R1)
		// Read value from albedo texture into diffuse.
		// If textured, use the texture; if not, use Material.diffuse.
		// Note: You can probably reuse parts of the radiosity assignment.

		// fetch barycentric coordinates
		float alpha = hit.v;
		float beta = hit.u;

		if (mat->textures[MeshBase::TextureType_Diffuse].exists())
		{
			// read diffuse texture like in assignment1

			const Texture& tex = mat->textures[MeshBase::TextureType_Diffuse];
			const Image& teximg = *tex.getImage();
			auto uv = alpha * hit.tri->m_vertices[2].t + beta * hit.tri->m_vertices[1].t + (1 -
				alpha - beta) * hit.tri->m_vertices[0].t;
			Vec2i texelCoords = getTexelCoords(uv, teximg.getSize());
			// apply gamma correction
			auto rawDiffuse = teximg.getVec4f(texelCoords).getXYZ();
			diffuse = Vec3f(pow(rawDiffuse.x, gamma), pow(rawDiffuse.y, gamma), pow(rawDiffuse.z, gamma));
		}
		else
		{
			// no texture, use constant albedo from material structure.
			// (this is just one line)
			diffuse = mat->diffuse.getXYZ();
		}
	}


	PathTracerContext::PathTracerContext()
		: m_bForceExit(false),
		  m_bResidual(false),
		  m_scene(nullptr),
		  m_rt(nullptr),
		  m_light(nullptr),
		  m_pass(0),
		  m_bounces(0),
		  m_destImage(0),
		  m_camera(nullptr)
	{
	}

	PathTracerContext::~PathTracerContext()
	{
	}


	PathTraceRenderer::PathTraceRenderer()
	{
		m_raysPerSecond = 0.0f;
	}

	PathTraceRenderer::~PathTraceRenderer()
	{
		stop();
	}

	inline void sampleReflection(float& pdf, Vec3f& w, float& cosTheta, Random& R, const Vec3f normal)
	{
		float x, y;
		do
		{
			x = R.getF64(-1.0f, 1.0f);
			y = R.getF64(-1.0f, 1.0f);
		}
		while (x * x + y * y > 1);
		float z = sqrt(1.0f - x * x - y * y);

		// Make the direction long but not too long to avoid numerical instability in the ray tracer.
		// For our scenes, 100 is a good length. (I know, this special casing sucks.)
		Mat3f B = formBasis(normal);
		w = (B * Vec3f(x, y, z)).normalized(6000.0f);
		cosTheta = dot(w, normal) / (w.length() * normal.length());
		pdf = cosTheta / FW_PI;
	}

	// This function traces a single path and returns the resulting color value that will get rendered on the image. 
	// Filling in the blanks here is all you need to do this time around.
	Vec3f PathTraceRenderer::tracePath(float image_x, float image_y, PathTracerContext& ctx, int samplerBase, Random& R,
	                                   std::vector<PathVisualizationNode>& visualization)
	{
		const MeshWithColors* scene = ctx.m_scene;
		RayTracer* rt = ctx.m_rt;
		Image* image = ctx.m_image.get();
		const CameraControls& cameraCtrl = *ctx.m_camera;
		AreaLight* light = ctx.m_light;
		const int bounces = ctx.m_bounces;
		const auto epsilon = 0.001f;
		const float terminationChance = 0.5f;
		const bool roulette = bounces < 0;


		// make sure we're on CPU
		image->getMutablePtr();

		// get camera orientation and projection
		Mat4f worldToCamera = cameraCtrl.getWorldToCamera();
		Mat4f projection = Mat4f::fitToView(Vec2f(-1, -1), Vec2f(2, 2), image->getSize()) * cameraCtrl.getCameraToClip();

		// inverse projection from clip space to world space
		Mat4f invP = (projection * worldToCamera).inverted();


		// Simple ray generation code, you can use this if you want to.

		// TODO: antialiasing by random sub-pixel samples
		// Generate a ray through the pixel.
		float x = (float)image_x / image->getSize().x * 2.0f - 1.0f;
		float y = (float)image_y / image->getSize().y * -2.0f + 1.0f;

		// point on front plane in homogeneous coordinates
		Vec4f P0(x, y, 0.0f, 1.0f);
		// point on back plane in homogeneous coordinates
		Vec4f P1(x, y, 1.0f, 1.0f);

		// apply inverse projection, divide by w to get object-space points
		Vec4f Roh = (invP * P0);
		Vec3f Ro = (Roh * (1.0f / Roh.w)).getXYZ();
		Vec4f Rdh = (invP * P1);
		Vec3f Rd = (Rdh * (1.0f / Rdh.w)).getXYZ();

		// Subtract front plane point from back plane point,
		// yields ray direction.
		// NOTE that it's not normalized; the direction Rd is defined
		// so that the segment to be traced is [Ro, Ro+Rd], i.e.,
		// intersections that come _after_ the point Ro+Rd are to be discarded.
		Rd = Rd - Ro;

		// trace!
		RaycastResult result = rt->raycast(Ro, Rd);
		const RTTriangle* pHit = result.tri;

		// if we hit something, fetch a color and insert into image
		Vec3f Ei;
		Vec3f Ej;
		Vec3f throughput(1, 1, 1);
		float p = 1.0f;
		const float contrib = 1.0f / terminationChance;


		if (result.tri != nullptr)
		{
			// TODO: YOUR CODE HERE (R2-R4):
			// Implement path tracing with direct light and shadows, scattering and Russian roulette.

			Vec3f diffuse;
			Vec3f specular;
			Vec3f pLight;

			float alpha = result.u;
			float beta = result.v;
			Vec3f normal =
				(alpha * pHit->m_vertices[1].n +
				beta * pHit->m_vertices[2].n +
				(1 - alpha - beta) * pHit->m_vertices[0].n).normalized();
			getTextureParameters(result, diffuse, normal, specular);
			Vec3f BRDF = diffuse * (1.0f / FW_PI);
			Vec3f BRDF1 = diffuse * (1.0f / FW_PI);

			float pdf1;
			int bounce = 0;
			bool running = true;

			float cosTheta = 0;
			Vec3f hitPoint = result.point;
			while (running)
			{
				Ej = 0;
				light->sample(pdf1, pLight, samplerBase, R);
				// construct vector from hitpoint to light sample
				auto l = pLight - hitPoint;
				//apply epsilon (fixes the demonic holes which took me hours to realize)
				auto rayfrom1 = hitPoint + epsilon * l.normalized();

				// trace shadow ray to see if it's blocked
				if (!ctx.m_rt->raycast(rayfrom1, l))
				{
					// if not, add the appropriate emission, 1/r^2 and clamped cosine terms, accounting for the PDF as well.
					// accumulate into E
					auto cos1 = dot(l, normal) / l.length();
					cos1 = cos1 < 0 ? 0 : cos1;
					auto cos2 = dot(-l, ctx.m_light->getNormal()) / (l.length() * ctx.m_light->getNormal().length());
					cos2 = cos2 < 0 ? 0 : cos2;
					// this is the hemisphere-to-area variable change! 
					auto G = cos2 * (1.0f / l.length() / l.length());
					Ej =
						ctx.m_light->getEmission() *
						BRDF *
						cos1 *
						G /
						pdf1;
				}
				// BRDF = BRDF1;
				Ei += throughput * Ej;
				if (bounce > abs(bounces))
				{
					if(roulette && R.getF32(0.0f, 1.0f) > terminationChance)
					{
						throughput *= contrib;
					}
					else
						break;
				}
				Vec3f omega;
				sampleReflection(p, omega, cosTheta, R, normal);
				// nudge ray origin to avoid holes
				auto rayfrom2 = hitPoint + epsilon * omega.normalized();
				// Shoot ray, see where we hit
				result = ctx.m_rt->raycast(rayfrom2, omega);
				if (result.tri != nullptr)
				{
					hitPoint = result.point;
					pHit = result.tri;
					alpha = result.u;
					beta = result.v;
					normal =
						(alpha * pHit->m_vertices[1].n +
						beta * pHit->m_vertices[2].n +
						(1 - alpha - beta) * pHit->m_vertices[0].n).normalized();
					getTextureParameters(result, diffuse, normal, specular);
					BRDF1 = diffuse * (1.0f / FW_PI);
					throughput *= BRDF1 * cosTheta * (1.0f/p);
				}
				else
				{
					break;
				}
				bounce++;
			}


			if (debugVis)
			{
				// Example code for using the visualization system. You can expand this to include further bounces, 
				// shadow rays, and whatever other useful information you can think of.
				PathVisualizationNode node;
				node.lines.push_back(PathVisualizationLine(result.orig, result.point)); // Draws a line between two points
				node.lines.push_back(PathVisualizationLine(result.point, result.point + result.tri->normal() * .1f,
				                                           Vec3f(1, 0, 0))); // You can give lines a color as optional parameter.
				node.labels.push_back(PathVisualizationLabel(
					"diffuse: " + std::to_string(Ei.x) + ", " + std::to_string(Ei.y) + ", " + std::to_string(Ei.z),
					result.point)); // You can also render text labels with world-space locations.

				visualization.push_back(node);
			}
		}

		return Ei;
	}

	inline float boxFilter(float x, float y)
	{
		auto x_ = x >= -0.5 && x <= 0.5 ? 1 : 0;
		auto y_ = y >= -0.5 && y <= 0.5 ? 1 : 0;
		return x_ * y_;
	}

	// This function is responsible for asynchronously generating paths for a given block.
	void PathTraceRenderer::pathTraceBlock(MulticoreLauncher::Task& t)
	{
		PathTracerContext& ctx = *(PathTracerContext*)t.data;

		const MeshWithColors* scene = ctx.m_scene;
		RayTracer* rt = ctx.m_rt;
		Image* image = ctx.m_image.get();
		const CameraControls& cameraCtrl = *ctx.m_camera;
		AreaLight* light = ctx.m_light;
		constexpr int samplesPerPixel = 9;

		// make sure we're on CPU
		image->getMutablePtr();

		// get camera orientation and projection
		Mat4f worldToCamera = cameraCtrl.getWorldToCamera();
		Mat4f projection = Mat4f::fitToView(Vec2f(-1, -1), Vec2f(2, 2), image->getSize()) * cameraCtrl.getCameraToClip();

		// inverse projection from clip space to world space
		Mat4f invP = (projection * worldToCamera).inverted();

		// get the block which we are rendering
		PathTracerBlock& block = ctx.m_blocks[t.idx];

		// Not used but must be passed to tracePath
		std::vector<PathVisualizationNode> dummyVisualization;

		static std::atomic<uint32_t> seed = 0;
		uint32_t current_seed = seed.fetch_add(1);
		Random R(t.idx + current_seed); // this is bogus, just to make the random numbers change each iteration

		// prob. to select point (x,y)
		float p_xy = 1.0;
		std::vector<float> weights(block.m_width * block.m_height);

		for (int i = 0; i < block.m_width * block.m_height; ++i)
		{
			if (ctx.m_bForceExit)
			{
				return;
			}

			// Use if you want.
			int pixel_x = block.m_x + (i % block.m_width);
			int pixel_y = block.m_y + (i / block.m_width);

			float sub_x, sub_y;
			Vec3f Ei;
			float w = 0;
			Vec3f Lout;
			for (int j = 0; j < samplesPerPixel; j++)
			{
				sub_x = R.getF32(pixel_x - 0.5, pixel_x + 0.5);
				sub_y = R.getF32(pixel_y - 0.5, pixel_y + 0.5);
				Ei = tracePath(sub_x, sub_y, ctx, 0, R, dummyVisualization);
				for (int k = 0; k < block.m_width * block.m_height; ++k)
				{
					int x_j = block.m_x + (k % block.m_width);
					int y_j = block.m_y + (k / block.m_width);
					float f_j = boxFilter(sub_x - x_j, sub_y - y_j);
					if (f_j != 0)
					{
						Lout += f_j * Ei;
						w += f_j;
					}
				}
			}
			Vec4f prev = image->getVec4f(Vec2i(pixel_x, pixel_y));
			prev += Vec4f(Lout / w, 1.0f);
			image->setVec4f(Vec2i(pixel_x, pixel_y), prev);
		}
	}

	void PathTraceRenderer::startPathTracingProcess(const MeshWithColors* scene, AreaLight* light, RayTracer* rt,
	                                                Image* dest, int bounces, const CameraControls& camera)
	{
		FW_ASSERT( !m_context.m_bForceExit );

		m_context.m_bForceExit = false;
		m_context.m_bResidual = false;
		m_context.m_camera = &camera;
		m_context.m_rt = rt;
		m_context.m_scene = scene;
		m_context.m_light = light;
		m_context.m_pass = 0;
		m_context.m_bounces = bounces;
		m_context.m_image.reset(new Image(dest->getSize(), ImageFormat::RGBA_Vec4f));

		m_context.m_destImage = dest;
		m_context.m_image->clear();

		// Add rendering blocks.
		m_context.m_blocks.clear();
		{
			int block_size = 32;
			int image_width = dest->getSize().x;
			int image_height = dest->getSize().y;
			int block_count_x = (image_width + block_size - 1) / block_size;
			int block_count_y = (image_height + block_size - 1) / block_size;

			for (int y = 0; y < block_count_y; ++y)
			{
				int block_start_y = y * block_size;
				int block_end_y = FW::min(block_start_y + block_size, image_height);
				int block_height = block_end_y - block_start_y;

				for (int x = 0; x < block_count_x; ++x)
				{
					int block_start_x = x * block_size;
					int block_end_x = FW::min(block_start_x + block_size, image_width);
					int block_width = block_end_x - block_start_x;

					PathTracerBlock block;
					block.m_x = block_size * x;
					block.m_y = block_size * y;
					block.m_width = block_width;
					block.m_height = block_height;

					m_context.m_blocks.push_back(block);
				}
			}
		}

		dest->clear();

		// Fire away!

		// If you change this, change the one in checkFinish too.
		m_launcher.setNumThreads(m_launcher.getNumCores());
		//m_launcher.setNumThreads(1);

		m_launcher.popAll();
		m_launcher.push(pathTraceBlock, &m_context, 0, (int)m_context.m_blocks.size());
	}

	void PathTraceRenderer::updatePicture(Image* dest)
	{
		FW_ASSERT( m_context.m_image != 0 );
		FW_ASSERT( m_context.m_image->getSize() == dest->getSize() );

		for (int i = 0; i < dest->getSize().y; ++i)
		{
			for (int j = 0; j < dest->getSize().x; ++j)
			{
				Vec4f D = m_context.m_image->getVec4f(Vec2i(j, i));
				if (D.w != 0.0f)
					D = D * (1.0f / D.w);

				// Gamma correction.
				Vec4f color = Vec4f(
					FW::pow(D.x, 1.0f / 2.2f),
					FW::pow(D.y, 1.0f / 2.2f),
					FW::pow(D.z, 1.0f / 2.2f),
					D.w
				);

				dest->setVec4f(Vec2i(j, i), color);
			}
		}
	}

	void PathTraceRenderer::checkFinish()
	{
		// have all the vertices from current bounce finished computing?
		if (m_launcher.getNumTasks() == m_launcher.getNumFinished())
		{
			// yes, remove from task list
			m_launcher.popAll();

			++m_context.m_pass;

			// you may want to uncomment this to write out a sequence of PNG images
			// after the completion of each full round through the image.
			//String fn = sprintf( "pt-%03dppp.png", m_context.m_pass );
			//File outfile( fn, File::Create );
			//exportLodePngImage( outfile, m_context.m_destImage );

			if (!m_context.m_bForceExit)
			{
				// keep going

				// If you change this, change the one in startPathTracingProcess too.
				m_launcher.setNumThreads(m_launcher.getNumCores());
				//m_launcher.setNumThreads(1);

				m_launcher.popAll();
				m_launcher.push(pathTraceBlock, &m_context, 0, (int)m_context.m_blocks.size());
				//::printf( "Next pass!" );
			}
			else ::printf("Stopped.");
		}
	}

	void PathTraceRenderer::stop()
	{
		m_context.m_bForceExit = true;

		if (isRunning())
		{
			m_context.m_bForceExit = true;
			while (m_launcher.getNumTasks() > m_launcher.getNumFinished())
			{
				Sleep(1);
			}
			m_launcher.popAll();
		}

		m_context.m_bForceExit = false;
	}
} // namespace FW
